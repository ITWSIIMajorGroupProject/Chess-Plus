Chess Plus
==========

This project is an initiative to develop an Online Live Chess Game by Additya Popli, Devansh Gautam, Kshitij Gupta, Pratik Jain and Trunapushpa.

Description
-----------

This project is built in Node.JS. This is a live online gaming platform.

Libraries used:
-	Express
-	Mongoose
-	Socket.io
-   Stockfish
-	Chessboard
-	Passport
-	passportSocketIo and many more...

Requirements
------------

Node.js and npm instlled on the machine. MongoDB server setup on the machine. For deployment details see **deployment**
section.

How To Run
----------

```javascript
npm install
node server.js
```

Deployment
-----------

Deployed using pm2 and nginx.

To set up deployment environment follow the instructions mentioned here:
https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-ubuntu-16-04


Features
---------

-   Chat between friends
-	Play against friends
-   Play against AI
-	Search for users
-	Send friend requests
-	Rating system
-	Play against random opponents
-	User-friendly interface and many more...
