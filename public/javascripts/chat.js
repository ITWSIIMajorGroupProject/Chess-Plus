var Chat = function(handle, userOff, receiveFunction, newMessageHandler) {

	var chatObject = {};
	// var userDetails;
	// var socket = io();
	socket.on('connection', function(data) {
		console.log('Hello..');
	});
	var senderId;
	socket.on('online', function(data) {
		//alert(data);
		//userDetails = data;
		senderId = data['username'];
		// handle(data);
		console.log(data);

		
	});


	socket.on('disconnect', function() {
		console.log('disconnect');
	});

	socket.on('userOffline', function(data) {
		userOff(data);
	});



	// username, full_name, (online_status), unread_count, profilepic_path,
	socket.on('receiveMessage', function(data) {
		receiveFunction(data);
	});

	// send Message to a specific user

	var sendMessage = function(receiverId, message) {
		var data = {
			'sender': senderId,
			'receiver': receiverId,
			'message': message
		};
		socket.emit('message', data);
	};


	var logout = function() {
		console.log('bye');
		//socket.emit('logout', {});
		$.get('http://localhost:8080/logout');
	};


	var getMessage = function(data) {
		socket.emit('getMessage', {
			'username': data
		});
	}


	var setRead = function(data) {
		socket.emit('setRead', data);
	};


	socket.on('newMessage', function(data) {
		//socket.emit('storeNew');
		newMessageHandler(data);
	});


	// Time to give object its properties

	chatObject.sendMessage = sendMessage;
	chatObject.senderId = senderId;
	chatObject.logout = logout;
	chatObject.setRead = setRead;
	chatObject.getMessage = getMessage;
	return chatObject;
};
