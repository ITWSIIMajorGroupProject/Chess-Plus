window.onload = function() {
	var chart = new CanvasJS.Chart("chartContainer", {
		title: {
			text: "Match Stats"
		},
		theme: "theme2",
		animationEnabled: true,
		data: [{
			type: "doughnut",
			indexLabelFontFamily: "Garamond",
			indexLabelFontSize: 20,
			startAngle: 0,
			indexLabelFontColor: "dimgrey",
			indexLabelLineColor: "darkgrey",
			toolTipContent: "{y} %",

			dataPoints: [{
				y: 51,
				indexLabel: "Wins {y}%"
			}, {
				y: 45,
				indexLabel: "Loss {y}%"
			}, {
				y: 4,
				indexLabel: "Draws {y}%"
			}]
		}]

	});

	chart.render();
};
